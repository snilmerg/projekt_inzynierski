﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Projekt_Inzynierski.Model
{
    public class Item
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        [Unique]
        public string Name { get; set; }
        public int? NextID { get; set; }
        public int? PreviousID { get; set; } // dla oznaczenia przedmiotu jako kategorii ustawia sie previous na -1
        public string Content { get; set; }
    }
}