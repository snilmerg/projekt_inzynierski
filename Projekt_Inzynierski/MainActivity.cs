﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Projekt_Inzynierski
{
    [Activity(Label = "Projekt_Inzynierski", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var btnDatabase = FindViewById<Button>(Resource.Id.btnDatabase);
            var btnMenu = FindViewById<Button>(Resource.Id.btnMenu);
            

            btnDatabase.Click += delegate
            {
                StartActivity(typeof(Databasectivity));
            };

            btnMenu.Click += delegate
            {
                StartActivity(typeof(MenuActivity));
            };


        }
    }
}

