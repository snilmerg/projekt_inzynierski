﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Projekt_Inzynierski.Model;
using Android.Util;

namespace Projekt_Inzynierski
{
    [Activity(Label = "Database")]
    public class Databasectivity : Activity
    {
        ListView lstData;
        List<Item> lstSource = new List<Item>();
        Database db;

        private void LoadData()
        {
            lstSource = db.SelectTableItem();
            var adapter = new ListViewAdapter(this, lstSource);
            lstData.Adapter = adapter;
        }


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Database);

            //Create DataBase
            db = new Database();
            db.CreateDatabase();

            string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            Log.Info("DB_PATH", folder);

            // Load fields and buttons
            lstData = FindViewById<ListView>(Resource.Id.listView);

            var edtID = FindViewById<EditText>(Resource.Id.edtID);
            var edtName = FindViewById<EditText>(Resource.Id.edtName);
            var edtNextID = FindViewById<EditText>(Resource.Id.edtNextID);
            var edtPreviousID = FindViewById<EditText>(Resource.Id.edtPreviousID);
            var edtContent = FindViewById<EditText>(Resource.Id.edtContent);

            var btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            var btnEdit = FindViewById<Button>(Resource.Id.btnEdit);
            var btnDelete = FindViewById<Button>(Resource.Id.btnDelete);
            var btnStartData = FindViewById<Button>(Resource.Id.btnStartData);
            var btnDeleteAll = FindViewById<Button>(Resource.Id.btnDeleteAll);

            //LoadData
            LoadData();

            //Events
            btnAdd.Click += delegate
            {
                Item item = new Item()
                {
                    Name = edtName.Text,
                    NextID = int.Parse(edtNextID.Text),
                    PreviousID = int.Parse(edtPreviousID.Text),
                    Content = edtContent.Text
                };
                db.InsertIntoTableItem(item);
                LoadData();
            };

            btnEdit.Click += delegate {
                Item item = new Item()
                {
                    ID = int.Parse(edtID.Text),
                    Name = edtName.Text,
                    NextID = int.Parse(edtNextID.Text),
                    PreviousID = int.Parse(edtPreviousID.Text),
                    Content = edtContent.Text
                };
                db.UpdateTableItem(item);
                LoadData();
            };

            btnDelete.Click += delegate {
                Item item = new Item()
                {
                    ID = int.Parse(edtID.Text),
                    Name = edtName.Text,
                    NextID = int.Parse(edtNextID.Text),
                    PreviousID = int.Parse(edtPreviousID.Text),
                    Content = edtContent.Text
                };
                db.DeleteTableItem(item);
                LoadData();
            };

            btnStartData.Click += delegate {
                db.StartData();
                LoadData();
            };

            btnDeleteAll.Click += delegate {
                db.DeleteAllItems();
                LoadData();
            };


            lstData.ItemClick += (s, e) =>
            {
                //for (int i = 0; i < lstData.Count; i++)
                //{
                //    if (e.Position == i)
                //        lstData.GetChildAt(i).SetBackgroundColor(Android.Graphics.Color.DarkGray);
                //    else
                //        lstData.GetChildAt(i).SetBackgroundColor(Android.Graphics.Color.Transparent);
                //}
                //Binding Data
                var txtID = e.View.FindViewById<TextView>(Resource.Id.textView4);
                var txtName = e.View.FindViewById<TextView>(Resource.Id.textView1);
                var txtNextID = e.View.FindViewById<TextView>(Resource.Id.textView2);
                var txtPreviousID = e.View.FindViewById<TextView>(Resource.Id.textView6);
                var txtContent = e.View.FindViewById<TextView>(Resource.Id.textView3);

                //edtName.Text = txtName.Text;
                edtID.Text = txtID.Text;
                edtName.Text = txtName.Text;
                edtNextID.Text = txtNextID.Text;
                edtPreviousID.Text = txtPreviousID.Text;
                edtContent.Text = txtContent.Text;

            };

        }
    }
}