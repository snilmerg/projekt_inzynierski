﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Android.Util;

namespace Projekt_Inzynierski.Model
{
    class Database
    {
        string Folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public bool CreateDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.CreateTable<Item>();
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return false;
            }

        }

        public bool InsertIntoTableItem(Item item)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.Insert(item);
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return false;
            }

        }

        public List<Item> SelectTableItem()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    return connection.Table<Item>().ToList();
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return null;
            }

        }

        public IEnumerable<Item> SelectItemsById(int id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    return connection.Table<Item>().ToList().Where(x => x.ID == id);
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return null;
            }

        }

        public IEnumerable<Item> SelectItemsByPreviousId(int id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    return connection.Table<Item>().ToList().Where(x => x.PreviousID == id);
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return null;
            }

        }

        public bool DeleteAllItems()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.DropTable<Item>();
                    connection.CreateTable<Item>();
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool StartData()
        {

            try
            {
                // KATEGORIE previd = -1
                InsertIntoTableItem(new Item() { Name = "kategorie_czynnosci", NextID = 0, PreviousID = -1, Content = "Chcę " });
                InsertIntoTableItem(new Item() { Name = "kategorie_konsumpcja", NextID = 0, PreviousID = -1, Content = "Mam ochotę na " });
                InsertIntoTableItem(new Item() { Name = "kategorie_miejsca", NextID = 0, PreviousID = -1, Content = "Chcę iśc do " });
                InsertIntoTableItem(new Item() { Name = "kategorie_mysli", NextID = 0, PreviousID = -1, Content = "Niech pomyślę... " });
                InsertIntoTableItem(new Item() { Name = "kategorie_osoby", NextID = 0, PreviousID = -1, Content = "Chcę do " });
                InsertIntoTableItem(new Item() { Name = "kategorie_ubrania", NextID = 0, PreviousID = -1, Content = "Chcę ubrać " });
                InsertIntoTableItem(new Item() { Name = "kategorie_uczucia", NextID = 0, PreviousID = -1, Content = "" });
                InsertIntoTableItem(new Item() { Name = "kategorie_zdrowie", NextID = 0, PreviousID = -1, Content = "" });
                InsertIntoTableItem(new Item() { Name = "kategorie_zwroty", NextID = 0, PreviousID = -1, Content = "" });

                //CZYNNOSCI previd = 1
                InsertIntoTableItem(new Item() { Name = "czynnosci_iscnadwor", NextID = 0, PreviousID = 1, Content = "iść na dwór" });
                InsertIntoTableItem(new Item() { Name = "czynnosci_przytulic", NextID = 0, PreviousID = 1, Content = "się przytulić" });
                InsertIntoTableItem(new Item() { Name = "czynnosci_spanie", NextID = 0, PreviousID = 1, Content = "iść spać" });
                InsertIntoTableItem(new Item() { Name = "czynnosci_zabawa", NextID = 0, PreviousID = 1, Content = "się bawić" });

                //KONSUMPCJA previd = 2
                InsertIntoTableItem(new Item() { Name = "konsumpcja_baton", NextID = 0, PreviousID = 2, Content = "batona" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_czekolada", NextID = 0, PreviousID = 2, Content = "czekoladę" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_deser", NextID = 0, PreviousID = 2, Content = "deser" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_kanapka", NextID = 0, PreviousID = 2, Content = "kanapkę" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_lizak", NextID = 0, PreviousID = 2, Content = "lizaka" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_lody", NextID = 0, PreviousID = 2, Content = "lody" });
                InsertIntoTableItem(new Item() { Name = "konsumpcja_obiad", NextID = 0, PreviousID = 2, Content = "obiad" });

                //MIEJSCA previd = 3
                InsertIntoTableItem(new Item() { Name = "miejsca_dom", NextID = 0, PreviousID = 3, Content = "domu" });
                InsertIntoTableItem(new Item() { Name = "miejsca_kosciol", NextID = 0, PreviousID = 3, Content = "kościoła" });
                InsertIntoTableItem(new Item() { Name = "miejsca_szkola", NextID = 0, PreviousID = 3, Content = "szkoły" });

                //MYSLI previd = 4
                InsertIntoTableItem(new Item() { Name = "mysli_ciekawe", NextID = 0, PreviousID = 4, Content = "a to ciekawe" });
                InsertIntoTableItem(new Item() { Name = "mysli_dobre", NextID = 0, PreviousID = 4, Content = "to jest niezłe" });
                InsertIntoTableItem(new Item() { Name = "mysli_dziwne", NextID = 0, PreviousID = 4, Content = "to jest dziwne" });
                InsertIntoTableItem(new Item() { Name = "mysli_fajne", NextID = 0, PreviousID = 4, Content = "to jest fajne" });
                InsertIntoTableItem(new Item() { Name = "mysli_niefajne", NextID = 0, PreviousID = 4, Content = "to jest niefajne" });
                InsertIntoTableItem(new Item() { Name = "mysli_niezgadzamsie", NextID = 0, PreviousID = 4, Content = "nie zgadzam się" });
                InsertIntoTableItem(new Item() { Name = "mysli_pomysl", NextID = 0, PreviousID = 4, Content = "mam pomysł" });
                InsertIntoTableItem(new Item() { Name = "mysli_zgadzamsie", NextID = 0, PreviousID = 4, Content = "zgadzam się" });
                InsertIntoTableItem(new Item() { Name = "mysli_zle", NextID = 0, PreviousID = 4, Content = "to jest złe" });

                //OSOBY previd = 5
                InsertIntoTableItem(new Item() { Name = "osoby_babcia", NextID = 0, PreviousID = 5, Content = "babci" });
                InsertIntoTableItem(new Item() { Name = "osoby_brat", NextID = 0, PreviousID = 5, Content = "brata" });
                InsertIntoTableItem(new Item() { Name = "osoby_dziadek", NextID = 0, PreviousID = 5, Content = "dziadka" });
                InsertIntoTableItem(new Item() { Name = "osoby_mama", NextID = 0, PreviousID = 5, Content = "mamy" });
                InsertIntoTableItem(new Item() { Name = "osoby_siostra", NextID = 0, PreviousID = 5, Content = "siostry" });
                InsertIntoTableItem(new Item() { Name = "osoby_tata", NextID = 0, PreviousID = 5, Content = "taty" });

                //UBRANIA previd = 6
                InsertIntoTableItem(new Item() { Name = "ubrania_czapka", NextID = 0, PreviousID = 6, Content = "czapkę" });
                InsertIntoTableItem(new Item() { Name = "ubrania_koszula", NextID = 0, PreviousID = 6, Content = "koszulę" });
                InsertIntoTableItem(new Item() { Name = "ubrania_rekawiczki", NextID = 0, PreviousID = 6, Content = "rękawiczki" });
                InsertIntoTableItem(new Item() { Name = "ubrania_spodenki", NextID = 0, PreviousID = 6, Content = "spodenki" });
                InsertIntoTableItem(new Item() { Name = "ubrania_spodnie", NextID = 0, PreviousID = 6, Content = "spodnie" });
                InsertIntoTableItem(new Item() { Name = "ubrania_szalik", NextID = 0, PreviousID = 6, Content = "szalik" });

                //UCZUCIA previd = 7
                InsertIntoTableItem(new Item() { Name = "uczucia_bojesie", NextID = 0, PreviousID = 7, Content = "Boję się" });
                InsertIntoTableItem(new Item() { Name = "uczucia_glod", NextID = 0, PreviousID = 7, Content = "Jestem głodny" });
                InsertIntoTableItem(new Item() { Name = "uczucia_goraco", NextID = 0, PreviousID = 7, Content = "Gorąco mi" });
                InsertIntoTableItem(new Item() { Name = "uczucia_kocham", NextID = 0, PreviousID = 7, Content = "Kocham Cię!" });
                InsertIntoTableItem(new Item() { Name = "uczucia_radosc", NextID = 0, PreviousID = 7, Content = "Hurra!" });
                InsertIntoTableItem(new Item() { Name = "uczucia_smutek", NextID = 0, PreviousID = 7, Content = "Smutno mi" });
                InsertIntoTableItem(new Item() { Name = "uczucia_wstyd", NextID = 0, PreviousID = 7, Content = "Wstydzę się" });
                InsertIntoTableItem(new Item() { Name = "uczucia_zimno", NextID = 0, PreviousID = 7, Content = "Zimno mi" });
                InsertIntoTableItem(new Item() { Name = "uczucia_zlosc", NextID = 0, PreviousID = 7, Content = "Jestem zły" });

                //ZDROWIE previd = 8
                InsertIntoTableItem(new Item() { Name = "zdrowie_biegunka", NextID = 0, PreviousID = 8, Content = "Mam biegunkę" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_bolibrzuch", NextID = 0, PreviousID = 8, Content = "Boli mnie brzuch" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_boliglowa", NextID = 0, PreviousID = 8, Content = "Boli mnie głowa" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_bolinoga", NextID = 0, PreviousID = 8, Content = "Boli mnie noga" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_chory", NextID = 0, PreviousID = 8, Content = "Jestem chory" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_goraczka", NextID = 0, PreviousID = 8, Content = "Mam gorączkę" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_kaszel", NextID = 0, PreviousID = 8, Content = "Mam kaszel" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_katar", NextID = 0, PreviousID = 8, Content = "Mam katar" });
                InsertIntoTableItem(new Item() { Name = "zdrowie_zatwardzenie", NextID = 0, PreviousID = 8, Content = "Nie mogę się załatwić" });

                //ZWROTY previd = 9
                InsertIntoTableItem(new Item() { Name = "zwroty_dziekuje", NextID = 0, PreviousID = 9, Content = "Dziękuję" });
                InsertIntoTableItem(new Item() { Name = "zwroty_prosze", NextID = 0, PreviousID = 9, Content = "Proszę" });
                InsertIntoTableItem(new Item() { Name = "zwroty_przepraszam", NextID = 0, PreviousID = 9, Content = "Przepraszam" });


                return true;
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLite ex", ex.Message);
                return false;
            }
        }

        public bool IsEmpty()
        {
            IEnumerable<Item> x = SelectItemsById(-1);
            if (x.Count() == 0)
                return true;
            else
                return false;
        }

        public int ReturnIdByName(string name)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    List<Item> x = connection.Query<Item>("SELECT * FROM Item Where Name=?", name);
                    var y = x.First();
                    return y.ID;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return 0;
            }
        }


        public bool UpdateTableItem(Item item)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.Query<Item>("UPDATE Item set Name=?,NextID=?, PreviousID=?, Content=? Where ID=?", item.Name, item.NextID, item.PreviousID, item.Content, item.ID);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool DeleteTableItem(Item item)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.Delete(item);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool SelectQueryTableItem(int ID)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(Folder, "Database.db")))
                {
                    connection.Query<Item>("SELECT * FROM Item Where ID=?", ID);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

    }
}