﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Projekt_Inzynierski.Model;
using Android.Speech.Tts;
using Java.Util;
using Android.Util;
using Android.Nfc;
using System.Threading.Tasks;

namespace Projekt_Inzynierski
{
    [Activity(Label = "GridActivity")]
    public class MenuActivity : AppCompatActivity, TextToSpeech.IOnInitListener
    {
        Database db;
        private TextToSpeech tts;
        private GridView gridView;
        private GridViewAdapter adapter;
        private string akcja = "";
        private string[] GridViewName;
        private string[] GridViewString = //tutaj implementujemy zwrot nazw wszystkich kategorii
        {
            //"mysli", "osoby", "ubrania", "uczucia", "zdrowie", "zwroty","miejsca"
            //"czynnosci", "konsumpcja", "miejsca", "mysli", "osoby", "ubrania", "uczucia", "zdrowie", "zwroty"
        };
        private int[] GridViewImage = 
        {
            //Resource.Drawable.hehe, Resource.Drawable.hehe, Resource.Drawable.hehe, Resource.Drawable.hehe, Resource.Drawable.hehe,
            //Resource.Drawable.hehe, Resource.Drawable.hehe, Resource.Drawable.hehe, Resource.Drawable.hehe

            //Resource.Drawable.kategorie_mysli,
            //Resource.Drawable.kategorie_osoby, Resource.Drawable.kategorie_ubrania, Resource.Drawable.kategorie_uczucia, Resource.Drawable.kategorie_zdrowie,
            //Resource.Drawable.kategorie_zwroty,Resource.Drawable.kategorie_miejsca

            //Resource.Drawable.kategorie_czynnosci, Resource.Drawable.kategorie_konsumpcja, Resource.Drawable.kategorie_miejsca, Resource.Drawable.kategorie_mysli,
            //Resource.Drawable.kategorie_osoby, Resource.Drawable.kategorie_ubrania, Resource.Drawable.kategorie_uczucia, Resource.Drawable.kategorie_zdrowie,
            //Resource.Drawable.kategorie_zwroty
        };



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Menu);
            tts = new TextToSpeech(this, this);
            db = new Database();
            db.CreateDatabase();
            

            var txtLektor = FindViewById<TextView>(Resource.Id.textLektor);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);

            SetSupportActionBar(toolbar);
            SupportActionBar.Title = "Terapia zaburzeń mowy";
            LoadData(-1);
            LoadGrid();


            gridView.ItemClick += async (s, e) => {
                //Toast.MakeText(this, "Item: " + GridViewString[e.Position], ToastLength.Short).Show();
                akcja += GridViewString[e.Position];
                txtLektor.Text = akcja;
                //tts.Speak(akcja, QueueMode.Flush, null, null);
                Speak(akcja);
                //Log.Debug("--- zwrocono nazwe wybranej kategorii --- " + GridViewString[e.Position] + " ---", ToString()); // review all the languages available
                int id = db.ReturnIdByName(GridViewName[e.Position]);
                if(id >= 10)
                {
                    LoadData(-1);
                    akcja = "";
                    Task.Delay(1500).Wait();
                }
                else
                    LoadData(id);
                LoadGrid();
            };

        }

        public void LoadGrid()
        {
            adapter = new GridViewAdapter(this, GridViewString, GridViewImage);
            gridView = FindViewById<GridView>(Resource.Id.grid_view_image_text);
            gridView.Adapter = adapter;
            
        }

        public void OnInit([GeneratedEnum] OperationResult status)
        {
            if (status.Equals(OperationResult.Success))
            {
                foreach (var locale in tts.AvailableLanguages)
                {
                    Log.Debug("TAG for language: ", locale.Language); // review all the languages available
                    if (locale.Language == "pl")
                        tts.SetLanguage(locale);
                }
                //tts.Speak("jak się masz?", QueueMode.Flush, null, null);
            }
            else
                Log.Error("Searching for pl language..", status.ToString());
        }
        public int Speak(string text)
        {
            tts.Speak(text, QueueMode.Flush, null, null);
            return 1;
        }

        public void LoadData(int id)
        {
            Log.Debug("--- wrocono id wybranej kategorii --- " + id + " ---", ToString()); // review all the languages available
            var source = db.SelectItemsByPreviousId(id);
            int i = 0;
            GridViewString = new string[source.Count()];
            GridViewName = new string[source.Count()];
            GridViewImage = new int[source.Count()];
            foreach (var x in source)
            {
                int resource = (int)typeof(Resource.Drawable).GetField(x.Name).GetValue(null);
                GridViewString[i] = x.Content;
                GridViewName[i] = x.Name;
                GridViewImage[i] = resource;
                i++;
            }
        }

    }
}