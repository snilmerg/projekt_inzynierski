﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace Projekt_Inzynierski.Model
{
    public class GridViewAdapter : BaseAdapter
    {
        private Context context;
        private string[] GridViewString;
        private int[] GridViewImage;

        public GridViewAdapter(Context context, string[] GridViewString, int[] GridViewImage)
        {
            this.context = context;
            this.GridViewString = GridViewString;
            this.GridViewImage = GridViewImage;
        }

        public override int Count
        {
            get
            {
                return GridViewString.Length;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;
            LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);
            if (convertView == null)
            {
                view = new View(context);
                view = inflater.Inflate(Resource.Layout.Grid_ViewLayout, null);
                TextView txtView = view.FindViewById<TextView>(Resource.Id.textView);
                ImageView imgView = view.FindViewById<ImageView>(Resource.Id.imageView);
                txtView.Text = GridViewString[position];
                imgView.SetImageResource(GridViewImage[position]);
            }
            else
            {
                view = (View)convertView;
            }
            return view;
        }
    }
}