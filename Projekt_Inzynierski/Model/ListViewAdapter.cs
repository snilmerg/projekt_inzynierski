﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Projekt_Inzynierski.Model
{
    public class ListViewAdapter : BaseAdapter
    {
        private Activity activity;
        private List<Item> lstItem;

        public ListViewAdapter(Activity activity, List<Item> lstItem)
        {
            this.activity = activity;
            this.lstItem = lstItem;
        }


        public override int Count
        {
            get
            {
                return lstItem.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return lstItem[position].ID;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.List_ViewDataTemplate, parent, false);

            var txtID = view.FindViewById<TextView>(Resource.Id.textView4);
            var txtName = view.FindViewById<TextView>(Resource.Id.textView1);
            var txtContent = view.FindViewById<TextView>(Resource.Id.textView3);
            var txtNextID = view.FindViewById<TextView>(Resource.Id.textView2);
            var txtPreviousID = view.FindViewById<TextView>(Resource.Id.textView6);
            

            txtID.Text = "" + lstItem[position].ID;
            txtName.Text = lstItem[position].Name;
            txtContent.Text = lstItem[position].Content;
            txtNextID.Text = "" + lstItem[position].NextID;
            txtPreviousID.Text = "" + lstItem[position].PreviousID;

            return view;
        }
    }
}